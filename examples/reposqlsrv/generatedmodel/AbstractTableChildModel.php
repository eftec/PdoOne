<?php
/** @noinspection PhpIncompatibleReturnTypeInspection
 * @noinspection ReturnTypeCanBeDeclaredInspection
 * @noinspection DuplicatedCode
 * @noinspection PhpUnused
 * @noinspection PhpUndefinedMethodInspection
 * @noinspection PhpUnusedLocalVariableInspection
 * @noinspection PhpUnusedAliasInspection
 * @noinspection NullPointerExceptionInspection
 * @noinspection SenselessProxyMethodInspection
 * @noinspection PhpParameterByRefIsNotUsedAsReferenceInspection
 */
namespace sqlsrv\repomodel;
use eftec\PdoOne;
use Exception;

/**
 * Class TableChildModel. Copyright (c) Jorge Castro C. (https://github.com/EFTEC/PdoOne)<br>
 * Generated by PdoOne Version 2.26 Date generated Sat, 19 Feb 2022 23:27:09 -0300.<br>
 * <b>DO NOT EDIT THIS CODE. THIS CODE WILL SELF GENERATE.</b><br>
 * <pre>
 * $code=$pdoOne->generateAbstractModelClass({args});
 * </pre>
 */
abstract class AbstractTableChildModel
{
	/** @var int $idtablachildPK  */
	public $idtablachildPK;
	/** @var string $NameChild  */
	public $NameChild;
	/** @var int $idgrandchildFK  */
	public $idgrandchildFK;

	/** @var TableGrandChildModel $_idgrandchildFK manytoone */
	public $_idgrandchildFK;
	/** @var TableParentModel[] $_TableParent onetomany */
	public $_TableParent;


    /**
     * AbstractTableChildModel constructor.
     *
     * @param array|null $array
     */
    public function __construct($array=null)
    {
        if($array===null) {
            return;
        }
        foreach($array as $k=>$v) {
            $this->{$k}=$v;
        }
    }

    //<editor-fold desc="array conversion">
    public static function fromArray($array) {
        if($array===null) {
            return null;
        }
        $obj=new TableChildModel();
        		$obj->idtablachildPK=isset($array['idtablachildPK']) ?  $array['idtablachildPK'] : null;
		$obj->NameChild=isset($array['NameChild']) ?  $array['NameChild'] : null;
		$obj->idgrandchildFK=isset($array['idgrandchildFK']) ?  $array['idgrandchildFK'] : null;
        		$obj->_idgrandchildFK=isset($array['_idgrandchildFK']) ? 
			$obj->_idgrandchildFK=TableGrandChildModel::fromArray($array['_idgrandchildFK']) 
			: null; // manytoone
		($obj->_idgrandchildFK !== null) 
			and $obj->_idgrandchildFK->idgrandchildPK=&$obj->idgrandchildFK; // linked manytoone
		$obj->_TableParent=isset($array['_TableParent']) ?  
			$obj->_TableParent=TableParentModel::fromArrayMultiple($array['_TableParent']) 
			: null; // onetomany

        return $obj;
    }

    /**
     * It converts the current object in an array
     *
     * @return mixed
     */
    public function toArray() {
        return static::objectToArray($this);
    }

    /**
     * It converts an array of arrays into an array of objects.
     *
     * @param array|null $array
     *
     * @return array|null
     */
    public static function fromArrayMultiple($array) {
        if($array===null) {
            return null;
        }
        $objs=[];
        foreach($array as $v) {
            $objs[]=self::fromArray($v);
        }
        return $objs;
    }
    //</editor-fold>

} // end class
