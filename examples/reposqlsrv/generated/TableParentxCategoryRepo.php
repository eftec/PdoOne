<?php
/** @noinspection AccessModifierPresentedInspection
 * @noinspection PhpUnusedAliasInspection
 * @noinspection UnknownInspectionInspection
 * @noinspection PhpUnused
 * @noinspection ReturnTypeCanBeDeclaredInspection
 */
namespace reposqlsrv;
use sqlsrv\repomodel\TableParentxCategoryModel;
use Exception;

/**
 * Class TableParentxCategoryRepo Copyright (c) Jorge Castro C. (https://github.com/EFTEC/PdoOne)<br>
 * <ul>
 * <li>idtablaparentPKFK int </li>
 * <li>idcategoryPKFK int </li>
 * <li>_idcategoryPKFK MANYTOONE (TableCategoryModel)</li>
 * <li>_idtablaparentPKFK ONETOONE (TableParentModel)</li>
 * </ul>
 * Generated by PdoOne Version 2.26 Date generated Sat, 19 Feb 2022 23:27:10 -0300.<br>
 * <b>YOU CAN EDIT THIS CODE</b>. It is not replaced by the generation of the code, unless it is indicated<br>
 * <pre>
 * $code=$pdoOne->generateCodeClassRepo(''TableParentxCategory'',''reposqlsrv'','array('TableParent'=>'TableParentRepo','TableChild'=>'TableChildRepo','TableGrandChild'=>'TableGrandChildRepo','TableGrandChildTag'=>'TableGrandChildTagRepo','TableParentxCategory'=>'TableParentxCategoryRepo','TableCategory'=>'TableCategoryRepo','TableParentExt'=>'TableParentExtRepo',)',''sqlsrv\repomodel\TableParentxCategoryModel'','array(0=>array(0=>'idtablaparentPKFK',1=>'int',2=>NULL,),1=>array(0=>'idcategoryPKFK',1=>'int',2=>NULL,),2=>array(0=>'_idcategoryPKFK',1=>'MANYTOONE',2=>'TableCategoryModel',),3=>array(0=>'_idtablaparentPKFK',1=>'ONETOONE',2=>'TableParentModel',),)');
 * </pre>
 * @see TableCategoryModel
 * @see TableParentModel
 */
class TableParentxCategoryRepo extends AbstractTableParentxCategoryRepo
{
    const ME=__CLASS__;
    const MODEL= TableParentxCategoryModel::class;


}
